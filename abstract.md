Abstract (in English)
=====================

A workshop to teach the very basics of programming to teenagers.
(Age 15-18, and the content is geared towards girls, specifically, but should work for boys as well.)

The goal is to implement a very simple model of the lunar lander game, within 4 hours in Python (and it's Logo implementation, the turtle graphics module), in a few gradual steps.

The math and physics apparatus is simplified on purpose and fits the targeted age group:
  - very basic coordinate geometry concepts: pixel, point, circle, angle
  - and simple concepts from physics: projectile motion, current speed,  acceleration, gravity.

There should be no new math/physics concepts compared to their regular school curricula. (It's 9th grade physics in Hungary.)

Focus is on programming instead, but with the instant reward of visual feedback as well as building mathematical/physical mental model of the problem at hand.
Kids will learn some programming concepts, i.e.:

  - variables and value assigments to variables, operators and comparison
  - sequence of instructions
  - simple if..else branching
  - loops
  - generate random numbers, invoke library functions in general
  - removing parts of code temporarily by commenting it out
  - basic debug and troubleshooting
  - refactoring, move code into a newly introduced function (if time permits)

The lesson plan with the detailed breakdown of the 4 hours as well as the comments in the code are in Hungarian (for now).

