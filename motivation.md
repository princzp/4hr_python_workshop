# Motiváció lányoknak (15 perc)

A programozás nem volt mindig ennyire férfias szakma...

[Számítóközponti dolgozók 1](motivation/motivation_01.jpg)

[Számítóközponti dolgozók 2](motivation/motivation_02.jpg)

[Számítógépterem és dolgozója a 70-es évek elejéről](motivation/motivation_03.jpg)

[Mágnesszalagos adattár és a kezelője?](motivation/motivation_04.jpg)

## Bobbi Johnson
A szépségkirálynőből lett programozó.

[Bobbi Johnson mint szépségkirálynő és mint programozó](motivation/bobbi_johnson.jpg)

https://en.wikipedia.org/wiki/Bobbi_Johnson

## Katherine Johnson
A NASA űrprogramjának híres matematikusa.

Röppálya-számításokat végzett a legelső emberes küldetésektől az űrsikló-programig, és a Marsi expedíciókban is.

"Mondja meg, mikor és hol szeretne Földet érni, és én megmondom, mikor lőjék fel."

A Számolás joga (Hidden figures) c. film egyik főalakja a valóságban.

Az USA legmagasabb polgári kitüntetésének birtokosa.

Még életében számítóközpontot neveztek el róla.

[Katherine Johnson az íróasztalánál](motivation/katherine_johnson_john_glenn.png). Előtte a röppálya-számítás eszközei.

[Egy tipikus röppálya-számítás holdraszálláshoz](motivation/katherine_johnson_calculation.png)

[Katherine Johnson elnöki kitüntetést vesz át](motivation/katherine_johnson_pres_obama.jpg)

[Katherine Johnson a róla elnevezett számítóközpont előtt](motivation/katherine_johnson_computational_research_facility.jpg)

https://en.wikipedia.org/wiki/Katherine_Johnson

https://www.nasa.gov/content/katherine-johnson-biography

## Frances 'Poppy' Northcutt

A NASA repülésirányításában dolgozott, rendkívül fiatalon, már 25 évesen kezdett az Apollo-8 misszióban, ő volt az első női mérnök.

Később tagja volt annak a csapatnak, aki a balul sikerült Apollo-13 legénységet élve visszahozta a Földre.

A NASA-nál dolgozva került kapcsolatba a női egyenjogúsági és emberjogi mozgalmakkal. Ez annyira elkezdte érdekelni, hogy visszaült az iskolapadba, mérnökből jogász lett.

A Hold egyik kráterét (Poppy) róla nevezték el.

[Poppy Northcutt az Apollo 8 misszió irányítóközpontjában](motivation/poppy_northcutt_1_apollo8_1968.png)

[Poppy Northcutt korabeli újságcikkben](motivation/poppy_northcutt_news_article.jpg)

[Poppy Northcutt munka közben](motivation/poppy_northcutt_desk.jpg). Mellette a röppálya-számításhoz használt speciális földgömb, amit Katherine Johnson íróasztalán is láthattunk.

[Poppy Northcutt és a többi repülésirányító](motivation/poppy_northcutt_3.jpg)

[Poppy Northcutt a táblánál](motivation/poppy_northcutt_blackboard.jpg)

[Poppy Northcutt és hallgatósága 1](motivation/poppy_northcutt_2.png)

[Poppy Northcutt és hallgatósága 2](motivation/poppy_northcutt_lecture2.jpg)

[Híres idézet tőle](motivation/poppy_northcutt_4.jpg)

https://en.wikipedia.org/wiki/List_of_people_with_craters_of_the_Moon_named_after_them#N

https://en.wikipedia.org/wiki/Frances_Northcutt

## Margaret Hamilton
Az USA Apollo és egyéb űrprogramjainak meghatározó szoftvermérnöke.

https://en.wikipedia.org/wiki/Margaret_Hamilton_(scientist)

