# Holdraszállás
# <n>.lépés: <mit csinál ez a program>
# Készítette: Varga Boglárka

# Ide írjuk majd a programunkat.
import turtle
import random
import math
turtle.screensize(800,600)
turtle.bgcolor ("midnight blue")

#1.lépés vége
screen = turtle.Screen()
screen.addshape("urhajo4.gif")
screen.bgpic("hatter.gif")
    
def leszallas():
    
    urhajo = turtle.Turtle()

    urhajo.shape("urhajo4.gif")
    urhajo.hideturtle()   #->csak vonal
    urhajo.penup()  #->toll fel
    urhajo.pencolor("red")
    urhajo.pen(fillcolor="light yellow", pencolor="light yellow")
    urhajo.pensize("10")

#2.lépés vége

    urhajo_x = random.randint(-400,-300)
    #random.randint( int(-1* (turtle.window_width() /2)) , int(-3/4*(turtle.window_width() /2)) )
    urhajo_y = random.randint(0.75*300,300)
    urhajo_sebesseg = 7
    urhajo.goto(urhajo_x, urhajo_y)
    urhajo.showturtle()
    urhajo.pendown()  #->toll le

#3.lépés vége

    #while urhajo_y >= -270:
        #urhajo.goto(urhajo_x, urhajo_y)
        #urhajo_x = urhajo_x + 1
        #urhajo_y = urhajo_y - 1

#4.lépés vége
   
    g = 3.711
    szog_fok = 30
    t = 0
    while urhajo_y >= -270:
        urhajo.goto(urhajo_x, urhajo_y)
        #print(urhajo_x)
        urhajo_x = urhajo_x + (urhajo_sebesseg * t * math.cos(math.radians(szog_fok)))
        urhajo_y = urhajo_y + (urhajo_sebesseg * t * math.sin(math.radians(szog_fok)) - (0.5 * g * t**2))
        t =t + 0.1
        if urhajo_y > 0:
            urhajo.pencolor("green")
            urhajo_sebesseg = 7
        elif urhajo_y >= -0.45*turtle.window_height()/2:
            urhajo.pencolor("yellow")
            urhajo_sebesseg = 5
        else:
            urhajo.pencolor("red")
            urhajo_sebesseg = 3


#5.lépés vége
#6.lépés vége  ->if-től
leszallas()
leszallas()
leszallas()
leszallas()
leszallas()
leszallas()
#7.lépés vége  ->def leszallas

# Program vége, de némely könyezetben azonnal be is zárná az ablakot.
# Ezért megállítjuk, és a konzol ablakban leütött Enter-re várunk.
# input("Program vége, nyomj Enter-t...")
