# Ez megjegyzés, a fordítóprogram el sem olvassa azokat a sorokat, amelyek így kezdődnek: #
# Mire jó? Írhatsz magyarázatot egy másik programozónak, aki majd a programodat olvassa, módosítja.
# Jegyzetelhetsz is így. Akár magyarul is.
# Még egy dologra hasznos: ha egy érvényes utasítás elé # jelet teszel, azzal ideiglenesen eltávolítod a programból, de könnyű lesz majd visszatenni.
# Ez hibakeresésnél, próbálgatásnál lesz nagyon hasznos. Mindjárt látni fogod.

'''
Többsoros megjegyzésre egy másik példa.
Három aposztróffal kezdődik és végződik, de minden egyes sor elejére nem kell (sőt nem szabad) kitenni.
Ilyennel is gyakran fogsz találkozni.
(Például a Python dokumentációjában.)
'''

# Így fog kinézni minden Python turtle programunk, 
# Érdemes ezt az ablakot nyitva tartani, és majd innen másolni a programunkba.
# Ez most nem csalás, ezt szabad.

import turtle
turtle.screensize(800,600)
turtle.bgcolor("aqua")

# ...
# Ide írjuk a programunkat

# Program vége, de némely könyezetben azonnal be is zárná az ablakot.
# Ezért megállítjuk, és a konzol ablakban leütött Enter-re várunk.
# input("Program vége, nyomj Enter-t...")
