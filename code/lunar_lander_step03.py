# Holdraszállás
# 3. lépés: véletlenszámok bevezetése.
# Űrhajót letenni a kezdőpozíciójába, a játékmező bal felső negyedébe, bárhova.
# Készítette: <írd ide a neved>

# Ide írjuk majd a programunkat.

import turtle
import random

turtle.screensize(800,600)
turtle.bgcolor("grey")

urhajo = turtle.Turtle()
# urhajo.hideturtle()
# urhajo.penup()
urhajo.shape("circle")
urhajo.pen(fillcolor="pink", pencolor="blue")

# Az űrhajó mozgását leíró változók
urhajo_x = random.randint(-400,-300) # random.randint( int(-1* (turtle.window_width() /2)) , int(-3/4*(turtle.window_width() /2)) )
urhajo_y = random.randint(0.75*300, 300)  # random.randint( int(3/4*(turtle.window_height()/2)) , int(   1*(turtle.window_height()/2)) )
# urhajo_sebesseg = 7     # tapasztalati érték, játssz vele!

urhajo.goto(urhajo_x, urhajo_y)
urhajo.showturtle()
urhajo.pendown() # így ki is rajzolja a röppályát, nem csak az aktuális pozíciót

# Program vége, de némely könyezetben azonnal be is zárná az ablakot.
# Ezért megállítjuk, és a konzol ablakban leütött Enter-re várunk.
# input("Program vége, nyomj Enter-t...")
