(2018-Apr-13_201403)

[Abstract (in English)](./abstract.md)

# Programozás kedvcsináló 15-18 éveseknek, 4 órában.

Kedvcsináló bevezető foglalkozás a számítógép-programozás világába a Python nyelven keresztül.
Önálló programozás, elmélet és gyakorlat sűrűn váltogatva.

A tervezett időtartam 4 tanóra. 
Rövid szünetekkel 4x60 perc, kb. 4 iskolai tanóra. A megcélzott korosztály 15-18 évesek.

Ha van rá idő és a gyerekek többségében lányok, akkor az alábbi motivációs résszel lehet kezdeni, kb. 15 percben.
Ha viszont csak szűkösen maradna idő Python-ozni, akkor ezt a részt el lehet hagyni, és rögtön a ferde hajítást előkészíteni.

[Motiváció, női példaképek és a ferde hajítás feladat előkészítése](./motivation.md)

# Óraterv a Python foglalkozáshoz

## Ismerkedés a Python nyelvi környezettel (0+10 = 10 perc)

Előadó: Logo nyelv, Turtle modul Python-ban

Gyakorlat:
Indítsuk el a Python IDLE környezetet.

Nyissuk meg a [bevezetes_1.py](code/bevezetes_1.py) programot.
Futtassuk le az F5 billentyűvel, vagy a Run->Run Module paranccsal a menüből.

Nyissuk meg a [bevezetes_2.py](code/bevezetes_2.py) programot.
Olvassuk el a szöveget figyelmesen.
Futtassuk le az F5 billentyűvel, vagy a Run->Run Module paranccsal a menüből.

Vegyük észre, hogy mindkét Python programunk meg van nyitva 1-1 ablakban.
Ez hasznos, tudunk az egyikbe dolgozni, a másikból másolni.
Ezt ki is fogjuk használni.

Vegyük észre, IDLE felismeri a kulcsszavakat, színezi amit felismer.
Próbáljuk ki a Ctrl-Space (autocomplete) funkciót, valamennyi gépelést meg lehet vele takarítani.

## Ferde hajítás (projectile motion) feladatban elmerülni (10+10 = 20 perc)

Kicsit körbejárni, ismerkedni vele. Hasznos linkek hátul a sillabusz végén is szerepelnek, de ezen a ponton megnézni, játszani vele:

* [Ferde hajítás példák](https://en.wikipedia.org/wiki/Trajectory_of_a_projectile)

* [Parabola-mozgás vektorok összegeként](http://www.physicsclassroom.com/mmedia/vectors/bds.cfm)

* [Nagyon jó interaktív szimuláció GeoGebra-ban](https://www.geogebra.org/m/KGxHnE55)

A NAT szerint ez kilencedikes anyag, suliban szerepelt már? Nem baj, ha esetleg még nem.

Rengeteg játék alapja:

  * tüzéres-ágyúzós-lövöldözős
  * bombavetés időzítése
  * ejtőernyősök indítása
  * angry birds
  * fruit ninja
  * holdraszállós-beesős (lunar lander!), ezt írjuk meg most

A programunk viselkedését az alábbi ábra szemlélteti.

Az űrhajónk a kékkel jelölt téglalap (a vászon bal felső 25-25%-nyi részének) egy véletlenszerűen kiválasztott pontján lép be a légtérbe, valamekkora szögben, valamekkora kezdősebességgel.

A leszállásnak legyen 3 fázisa (rendre zöld, sárga és piros) aszerint, hogy mennyire van magasan az űrhajó a landolás tervezett helyétől.
Például ezeken a pontokon kell szabadeséssel bezuhanni, ejtőernyőt nyitni, fékezőrakétákat indítani, stb.:

* zöld fázis: amikor az űrhajó y koordinátája még pozitív
* sárga fázis: a vászon alsó 45%-án
* piros fázis: különben, azaz -45% és -90% között

![Fig. 1 Holdraszállás ábra.](figures/fig_lunar_lander.png)

A holdraszállós programunkat több (7) lépésben írjuk meg. A workshop célja legalább az 5. lépés végéig eljutni. Ez már egy kerek egész, sikerélményt adó programozási feladat.
Ha az idő engedi, folytassuk a további lépésekkel.

### 1. lépés: játékmező (vászon, amire rajzolunk) létrehozása. (20+10 = 30 perc)

Dolgozzunk az előre elkészített [skeleton.py](code/skeleton.py) programból:

* nyissuk meg
* mentsük el (File -> Save as) a saját könyvtárunkba valami `azénnevem_1.py`-ra végződő néven, hogy későbbi visszanézésre megmaradjon akkor is, ha a következő lépésben jelentősen megváltozik.
* töltsük ki benne a nevünket
* írjuk hozzá az első lépés kódját (ld. lentebb)
* a végén (és közben sűrűn) ne felejtsünk el menteni

Ebben a lépésben az a feladat, hogy beimportáljuk a turtle modult és nyissunk egy kb. tenyérnyi vásznat, amire dolgozni fogunk.  
Felbontástól függően 800x600 pixel kb. már elég.  
Nem muszáj, de lehet háttérszínt beállítani.

Valami ilyesmit kell a skeleton kódba a megfelelő helyre beírni (kedvenc színt a Függelékben található színtáblázatból lehet választani, csak ne menjen el vele rengeteg idő, és a zöld, sárga, piros jól látszódjanak majd rajta):

```python
import turtle
turtle.screensize(800,600)
turtle.bgcolor("grey")
```

A kész program, ahová el kellene jutni ebben a lépésben: [1. lépés programja](code/lunar_lander_step01.py).  
Futtassuk is le, próbáljuk ki egyszer.  
Egy üres vásznat kell lássunk.

Mentsük el a programunkat (File -> Save as) a saját könyvtárunkba valami `azénnevem_2.py`-ra végződő néven, hogy későbbi visszanézésre megmaradjon akkor is, ha a következő lépésben jelentősen megváltozik.

### 2. lépés: legyen egy űrhajónk (turtle teknős) (30+30 = 60 perc)

A változó fogalmának bevezetése.

* Változó fogalma.
* Névadási szabályok.
* Változó típusa, azokon megengedett műveletek, `type()` függvény.
* Egészek és lebegőpontos számok.
* Típusok közötti konverziók, `float` és `int` közötti konverzió oda-vissza.
* Műveletek változókon.
* Értékadás (`=`).
* Inkrementálás vs. matek, `x = x+1` kifejezésnek miért van értelme, mit jelent.
* Változó értékének összehasonlítása, összehasonlító operátorok (`==`), logikai kifejezések.
* Nemlétező változóra hivatkozás.
* Változók állapotteret feszítenek ki.

Gyakorlat:
Hozzunk létre egy teknőst, alakja legyen kör. Legyen valamilyen színe a körvonalnak és legyen valamilyen kitöltési szín is.

Most még nem baj (sőt!) ha a teknős látszik és vonalat is húzna, ha mozogna.
(Ezek az alapértelmezések, nem is kell rendelkeznünk róluk, de egyébként a `turtle.showturtle()` és a `turtle.pendown()` használatával lehetne elérni.)
Később majd ezeket kikapcsoljuk, ha most be vannak kapcsolva. Diagnosztikai célból viszont most még hasznos: látjuk, mi történik, nem csak a végeredményt.

Valami ilyesmit kellene hozzáírni a kódhoz:

```python
urhajo = turtle.Turtle()
# urhajo.hideturtle()
# urhajo.penup()
urhajo.shape("circle")
urhajo.pen(fillcolor="pink", pencolor="blue")
```

A kész program, ahová el kellene jutni ebben a lépésben: [2. lépés programja](code/lunar_lander_step02.py).
Futtassuk is le, próbáljuk ki egyszer.  
Most már az üres vászon közepén a teknősünket (űrhajónkat) kell lássuk.

Mentsük el a programunkat (File -> Save as) a saját könyvtárunkba valami `azénnevem_3.py`-ra végződő néven, hogy későbbi visszanézésre megmaradjon akkor is, ha a következő lépésben jelentősen megváltozik.

### 3. lépés: véletlenszámok bevezetése, űrhajót letenni a kezdőpozíciójába, a játékmező bal felső részébe, bárhova. (60+30 = 90 perc)
A program elején, az importoknál, importáljuk a random modult is.

```python
import random
```

Vegyünk fel változókat az űrhajó x és y koordinátáinak, adjunk neki értéket a `random.randint()` segítségével.
A koodináták a bal felső részbe essenek, ekkor szépen látszik majd a szimuláció. A `randint()` paramétereit a választott vászonméretből kell kiszámolni,
vagy elegánsabban, képlettel, a `turtle.window_width()` és a `turtle.window_height()` függvények segítségével, amint az az alábbi példakód kommentezett részében szerepel.
(Később, a ferde hajításhoz fog kelleni egy sebesség is, felvehetjük azt is akár most.)
Az elkészült űrhajó menjen is el a neki kisorsolt pozícióra, jelenjen meg és kezdjen el vonalat húzni maga után.

Valami ilyesmit kellene hozzáírni a kódhoz:

```python
# Az űrhajó mozgását leíró változók
urhajo_x = random.randint(-400,-300) # random.randint( int(-1* (turtle.window_width() /2)) , int(-3/4*(turtle.window_width() /2)) )
urhajo_y = random.randint(0.75*300, 300)  # random.randint( int(3/4*(turtle.window_height()/2)) , int(   1*(turtle.window_height()/2)) )
# urhajo_sebesseg = 7     # tapasztalati érték, játssz vele!

urhajo.goto(urhajo_x, urhajo_y)
urhajo.showturtle()
urhajo.pendown() # így ki is rajzolja a röppályát, nem csak az aktuális pozíciót

```

A kész program, ahová el kellene jutni ebben a lépésben: [3. lépés programja](code/lunar_lander_step03.py).
Futtassuk is le, próbáljuk ki egyszer.
Azt kell látnunk, hogy a teknősünk a vászon közepéről a neki kisorsolt helyre megy, közben esetleg vonalat húz, ha azt bekapcsolva hagytuk.

Mentsük el a programunkat (File -> Save as) a saját könyvtárunkba valami `azénnevem_4.py`-ra végződő néven, hogy későbbi visszanézésre megmaradjon akkor is, ha a következő lépésben jelentősen megváltozik.

### 4. lépés: mindegy, hogy hogyan, csak mozogjon az űrhajónk (90+30 = 120 perc)
Itt most még nem lényeges, hogy a fizikai törvényszerűségeknek megfelelően mozogjon a test.
Elég, ha függőlegesen leesik, de még jobb, ha az x és y koordinátát is manipuláljuk.
Pl. "45 fokban" leesik, azaz 1 pixelt jobbra, 1 pixelt lefelé mozog a ciklusban, mint a futó a sakktáblán.

A ciklus fogalmának bevezetése.

* Programutasítások ismétlése: ciklusok.
* A ciklus fogalma. 
* Többféle ciklus, elöl- és hátultesztelő ciklusok.

Ezen a workshopon egyféle elöltesztelő ciklussal (`while` ciklus) fogunk csak találkozni.

```python
x = 1
while (x<6):
    print("hello", x)
    # FONTOS!!! A ciklusváltozót kézzel állítani!
    x = x + 1 # rövidebben: x+=1 
```

Elöltesztelő ciklus: lefuthat nullaszor, egyszer, többször vagy végtelenszer.

Nem volt még szó az elágazásokról, de érdemes előrebocsátani:
* Bizonyos értelemben hasonló az `if`-hez: megvizsgálja a feltételét, és ha az logikai igaz (`True`), akkor végrehajtja a törzsében (a ciklusmagban) lévű utasításokat.  
* De nem csak egyszer, mint az `if`, hanem a ciklusmag egy fordulata után újból kiértékeli a kifejezést, és ha az még mindig igaz, akkor újra végrehajtja a ciklusmagot, és így tovább, akár a végtelenségig.

Ezért:

* lefuthat nullaszor: ha a feltétele már kezdetben is hamis
* pontosan egyszer, mint az `if`, ha a feltétele kezdetben igaz, majd a ciklusmagban azonnal hamissá válik
* valahányszor (véges ciklus): ha a feltétel kezdetben igaz, és még egy darabig igaz
* végtelenszer, ha a feltétele kezdetben igaz és sose válik hamissá a ciklusmagban.

Példaprogram a magyarázathoz: [ciklus.py](code/ciklus.py).

Valamint [barkochba.py](code/barkochba.py) demonstrálja, hogy ezek egymásba ágyazhatók, mert itt a `while` ciklusban szerepel elágazás és értékadás is.

A kulcs ehhez a lépéshez, hogy a hallgató felismerje a ciklus (célszerűen a `while`) szükségességét.

Az űrhajót és a vonalhúzást most már kapcsoljuk ki addig, amíg el nem éri a neki kisorsolt pozíciót.

```python
urhajo.hideturtle()
urhajo.penup()
```

Valami ilyesmit kellene hozzáírni a kódhoz:

```python
while urhajo_y >= -270:    # -0.9*(turtle.window_height()/2):
    urhajo.goto(urhajo_x, urhajo_y)

    # első, naiv megközelítés: menjen "45 fokban",
    # azaz 1 pixelt jobbra, 1 pixelt le
    urhajo_x = urhajo_x + 1
    urhajo_y = urhajo_y - 1

    # ciklus vége

# ciklus utáni rész, ezt a kintebbi bekezdés jelzi

```

Ha esetleg végtelen ciklust sikerülne írni, nem kell megijedni, konzolban Ctrl-C megállítja a futó programot.

A kész program, ahová el kellene jutni ebben a lépésben: [4. lépés programja](code/lunar_lander_step04.py).  
Futtassuk is le, próbáljuk ki egyszer.  
Azt kell látnunk, hogy a teknősünk a kisorsolt helyről átlózva lefelé halad, majd megáll.

Mentsük el a programunkat (File -> Save as) a saját könyvtárunkba valami `azénnevem_5.py`-ra végződő néven, hogy későbbi visszanézésre megmaradjon akkor is, ha a következő lépésben jelentősen megváltozik.

### 5. lépés: a deszkamodellbe bevinni a fizikát, modellezés, megfeleltetés a valóságnak. A ferde hajítás mozgásegyenlete. (120+30 = 150 perc)
Ez a legnehezebb lépés, itt fel kell idézni a ferde hajításról (és esetleg a paraboláról) tanultakat.

https://en.wikipedia.org/wiki/Projectile_motion#Displacement

Minket most csak az elmozdulás érdekel:

```math
\Delta x = v * t * cos(\theta)
```

valamint

```math
\Delta y = v * t * sin(\theta) - \frac{1}{2} * g * t^2
```

Importáljuk a `math` modult a program elején, a többi import között.  
(Ebben vannak a `sin()`, `cos()` és `radians()` függvények, amikre szükségünk lesz.)

```python
import math
```

Az űrhajó x és y koordinátája mellett, legyen egy sebesség változója is. Ha korábban nem hoztuk létre, legkésőbb most tegyük meg:

```python
urhajo_sebesseg = 7     # tapasztalati érték, játssz vele!

```

Vegyük fel a képletből a ferde hajítás paramétereit:

* a gravitációs állandót valamilyen hihető értékkel, célszerűen 1.5 és 25 között
* a ferde hajítás (légtérbe belépés) szögét fokokban, lehet előjeles is, célszerűen -30 és +30 között
* az időt

```python
# A ferde hajítás paraméterei
g = 3.711       # gravitációs állandó. A Földön: 9.807 m/s², a Holdon: 1.622 m/s², a Marson: 3.711 m/s², A Jupiteren: 24.79 m/s². Tapasztalati érték, játssz vele!
szog_fok = 30   # légkörbe belépés szöge, tapasztalati érték, játssz vele!
t = 0           # eltelt (relatív) idő
```

A kulcslépés, hogy a naiv mozgásegyenletet cseréljük le a fizikai modellből következő mozgásegyenletre.  
Összesen 2 sort kell kicserélnünk, az x és y koordináta értékadását, de a következó, nem-triviális dolgokra kell ügyelni:

* a szöget fokokban adtuk meg, a képlet radiánt vár, a konverziót a `math.radians()` függvény végzi el
* ezzel a radián értékkel lehet `sin()`, ill. `cos()` értéket számolni, vagyis függvényt kell függvénybe ágyazni, pont mint matekórán az $`f(g(x))`$ függvénykompozícióval, ezért ettől sem kell megijedni, a sorrend viszont lényeges.
* a képlet az elmozdulásvektor komponenseit adja meg, vagyis ez nem az új koordináta, hanem amit hozzá kell adni a koordinátához

Mindezek alapján valami ilyesmit kellene hozzáírni a kódhoz:

```python
# első, naiv megközelítés: menjen "45 fokban",
# azaz 1 pixelt jobbra, 1 pixelt le
# urhajo_x = urhajo_x + 1
# urhajo_y = urhajo_y - 1
urhajo_x = urhajo_x + (urhajo_sebesseg * t * math.cos(math.radians(szog_fok)))
urhajo_y = urhajo_y + (urhajo_sebesseg * t * math.sin(math.radians(szog_fok)) - (0.5 * g * t**2))
```

A ciklusban most muszáj az eltelt t időt növelni, különben végtelen ciklushoz jutunk. Ez sem baj, konzolból Ctrl-C-vel meg tudjuk állítani és legalább láttunk ilyet is.
Bátran lehet 1-nél kisebb értékekkel is kísérletezni, ez egy szimulációs idő most, nem feltételnül kell SI mértékegységben lennie. Most ez a mozgást finomítja, kevésbé lesz pixeles a parabola.

```python
t = t + 0.1         # ha a mp felbontás túl gyors, akkor legyen 10x lassított felvétel
```

Ha szemmel követéshez esetleg még mindig túl gyors lenne a szimuláció, akkor importáljuk a `time` modult, és a `time.sleep(0.05)` -vagy valami hasonló- értékkel altassuk el a while ciklus legvégén.

```python
#import time     # amikor túl gyors, esetleg teszünk majd bele lassítást
...
    #time.sleep(0.05)   # ha még mindig túl gyors, további lassítás altatással
```

A kész program, ahová el kellene jutni ebben a lépésben: [5. lépés programja](code/lunar_lander_step05.py).
Futtassuk is le, próbáljuk ki egyszer.  
Azt kellene lássuk, hogy szépen megrajzolja a parabolát.

Ha idáig eljutunk, akkor már a workshop sikeresnek mondható, megvan a ferde hajítás.  
A további lépések opcionálisak. Ha az idő megengedi, haladjunk tovább.

Mentsük el a programunkat (File -> Save as) a saját könyvtárunkba valami `azénnevem_6.py`-ra végződő néven, hogy későbbi visszanézésre megmaradjon akkor is, ha a következő lépésben jelentősen megváltozik.

### 6. lépés: ciklusmagban elágazás (150+30 = 180 perc)
Az elágazás fogalmának bevezetése.

Feltételes utasítás-végrehajtás, elágazás
Logikai kifejezés fogalmának bevezetése, `True` és `False` logikai értékek.  
Kulcsszavak, Python nyelvi elemek: `if`, `else`.  
Szintaktikus cukorka: az `elif` kulcsszó:

```python
if feltétel1:
    pass
    else:
        if feltétel2:
        pass
        else:
            if feltétel3:
            pass
```

helyett (sok tabulálás, ronda szerkezet) egy elegánsabb szintaxis:

```python
if feltétel1:
    pass
elif feltétel2:
    pass
elif feltétel3:
    pass
```

Példaprogram a magyarázathoz: [elagazas.py](code/elagazas.py)

Ennek a lépésnek az is a didaktikai célja, hogy az imperatív programozás alkotóelemeit (étékadás, elágazás, ciklus) gyakoroljuk egymásba ágyazni.

Gyakorlat:
A ciklusmagunk most nagyon egyszerű, csak pár értékadás van benne. Most annyi a célunk, legyen a ciklusmagban egy elágazás is, az egyszerűség kedvéért feltételes értékadások.
Például a leszállásnak legyen 3 fázisa (rendre zöld, sárga és piros) aszerint, hogy mennyire van magasan az űrhajó a landolás tervezett helyétől.
(Például ezeken a pontokon kell fékezőrakétákat indítani, ejtőernyőt nyitni, stb.)
Egy lehetséges ötlet a feladatot leíró [ábrán](figures/fig_lunar_lander.png) is szerepel:

* zöld fázis: amikor az űrhajó y koordinátája még pozitív
* sárga fázis: a vászon alsó 45%-án
* piros fázis: különben, azaz -45% és -90% (a ciklus leállási feltétele) között

Változtassuk meg a `pencolor()` függvény segítségével ezen leszállási fázisok alapján a teknős filctoll színét:

```python
    if urhajo_y > 0:
        urhajo.pencolor("green")
    elif urhajo_y >= -0.45*turtle.window_height()/2:
        urhajo.pencolor("yellow")
    else:   # -45% és -90% között van, a pálya utolsó szakaszában tartózkodik
        urhajo.pencolor("red")
```

A kész program, ahová el kellene jutni ebben a lépésben: [6. lépés programja](code/lunar_lander_step06.py).  
Futtassuk is le, próbáljuk ki egyszer.  
Azt kellene lássuk, hogy szépen megrajzolja a parabolát, mint az előző lépésben, azonban három különböző színnel.

Ha még mindig maradt egy kis idő, de már nem elég a következő lépésre, vagy csak előrébb tart 1-2 hallgató, 
akkor fel lehet vetni az if ágakban egyéb értékadásokat, pl. a sárga és piros szakaszokban csökkenhet is a sebesség (a fékezőernyő, ill. a fékezőrakéta hatása miatt).
Ez megtöri a parabolát, de ki lehet próbálni.

Mentsük el a programunkat (File -> Save as) a saját könyvtárunkba valami `azénnevem_7.py`-ra végződő néven, hogy későbbi visszanézésre megmaradjon akkor is, ha a következő lépésben jelentősen megváltozik.

### 7. lépés: Refaktorálás, programrészek kiemelése függvénybe, függvény hívása (180+30 = 210 perc)

Függvény fogalmát bevezetni.  Nyelvi elem a kód kényelmes újrafelhasználására, blokkokba szervezésére, olvashatóbbá tételére.
Programrészek kiemelése függvénybe. A `def` kulcsszó.
Függvények hívása.

Az előző lépésben megírt program lényegi részét kell most egy függvénybe kiemelni.  
Kell egy új függvény a `def` kulcsszóval bevezetve (pl. `leszallas()`), és a az űrhajó (teknős) létrehozásától egészen a while ciklusmag végéig
(ahol a szimulált időt növeljük) beljebb tab-olni mindent ebbe a függvénybe, 
a Format -> Indent Region menüpont segítségével.

Valami ilyesmit kellene eredményül kapnunk az editorban:

```python
def leszallas():
    urhajo = turtle.Turtle()
    urhajo.hideturtle()
    ...
    while urhajo_y >= -0.9*(turtle.window_height()/2):
        ...
        t = t + 0.1         # mp felbontás túl gyors, legyen 10x lassított felvétel
        #time.sleep(0.05)   # ha még mindig túl gyors, további lassítás altatással
        
        # ciklus vége
    # függvény vége
# ciklus utáni rész, ezt a kintebbi bekezdés jelzi
```

Mentsük el, próbáljuk ki.  
Futtatás után nem csinál semmit, miért? Mert a főprogram most üres, az újonnan elkészült függvényünket még meg is kell hívni.  
Be kell tehát tenni pár hívást a függvényre, annak definíciója után, célszerűen a programszöveg legvégére.

```python
# Főprogram
leszallas()
leszallas()
leszallas()
```

Ha jól állunk idővel, ide is lehet egy ciklust tenni, és a ferde hajítások számát egy változóval vezérelni.

A kész program, ahová el kellene jutni ebben a lépésben: [7. lépés programja](code/lunar_lander_step07.py).  
Futtassuk is le, próbáljuk ki egyszer.  
Azt kellene lássuk, hogy annyi ferde hajítást végez, ahányszor a függvényt meghívtuk.

Ezeknek a ferde hajításoknak a fizikai paraméterei most azonosak, ezért ez egy párhuzamosan eltolt görbesereg.  
Ha még mindig maradt idő a foglalkozásból, és van kedvük játszani a gyerekeknek, lehet `random.randint()` hívásokra cserélni a most még behegesztett konstansokat, hogy izgalmasabb görbesereghez juthassunk, például:

```python
urhajo_sebesseg = 7     # tapasztalati érték, játssz vele!
```
helyett
```python
urhajo_sebesseg = random.randint(5,10)     # tapasztalati érték, játssz vele!
```
valamint
```python
szog_fok = 30   # légkörbe belépés szöge, tapasztalati érték, játssz vele!
```
helyett
```python
szog_fok = random.randint(10,30)   # légkörbe belépés szöge, tapasztalati érték, játssz vele!
```

## Összefoglalás, ismétlés
Miről volt szó? Honnan-hová jutottunk el?

Ez most csak egy prototípus, de az analitikus gondolkodást, a probléma részekre darabolását, 
a deszkamodellre a fizikai modell adaptálásást, stb., jól demonstrálja.

Mi hiányzik még egy komplett, kereskedelmi játékhoz?

* vezérlés billentyűzetről (bala, jobbra, felfelé nyíl)
* tüzelőanyag-szint figyelése
* pontszámítás, életek számának nyilvántartása
* tereptárgyak
* esztétika: igazi űrhajó, hangok, stb.

https://en.wikipedia.org/wiki/Lunar_Lander_(video_game_series)

http://moonlander.seb.ly/

Kérdések és válaszok, visszajelzések.

## Opcionális
Ha még mindig nagyon jól állunk idővel: PyCharm Edu bemutató, beépített Python minitanfolyamot megmutatni.

# Demo a szülőknek.

# Kellhet (hasznos linkek)

* [Ferde hajítás példák](https://en.wikipedia.org/wiki/Trajectory_of_a_projectile)

* [Parabola-mozgás vektorok összegeként](http://www.physicsclassroom.com/mmedia/vectors/bds.cfm)

* [Nagyon jó interaktív szimuláció GeoGebra-ban](https://www.geogebra.org/m/KGxHnE55)

* [Angol wikipedia szócikk a szükséges képletekkel](https://en.wikipedia.org/wiki/Projectile_motion#Displacement)

* [Sulinet tudásbázis a ferde hajításról](http://tudasbazis.sulinet.hu/hu/termeszettudomanyok/fizika/fizika-9-evfolyam/a-testek-halado-mozgasa/a-ferde-hajitas)

* [Wolfram Alpha a ferde hajításról](http://www.wolframalpha.com/input/?i=projectile+motion)

* https://docs.python.org/3.4/index.html

* https://docs.python.org/3.4/library/turtle.html

# Függelék

RGB színkeverő (webes): https://www.w3schools.com/colors/colors_picker.asp

![Szín nevek 1](etc/colornames_1.png)

![Szín nevek 2](etc/colornames_2.png)

![Szín nevek 3](etc/colornames_3.png)

![Szín nevek 4](etc/colornames_4.png)

